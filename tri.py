#!/usr/bin/env python

import os
import sys


fa = open("listea.txt","r")
fb = open("listeb.txt","r")

same = set(fa).difference(fb)
same.discard('\n')
with open('output.txt','w') as fo:
    for line in same:
        fo.write(line)

os.remove('listea.txt')
os.rename('output.txt','listea.txt')
